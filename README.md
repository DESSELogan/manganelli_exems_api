# Manganelli Exam Project

## Mise en place
### Prérequis
* Node 
* npm

### Clone et installation

```bash
git clone https://gitlab.com/DESSELogan/bot-des-rognons.git
```
```bash
npm install
```

Ajouter le fichier .env transmis dans le mail à la racine du projet

Lancer le projet avec 
```bash
npm start
```

Tester sur http://127.0.0.1:3000/api/stock/all_filtered
