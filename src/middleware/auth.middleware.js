const axios = require("axios");
global.jwt = null;

/**
 * Function that check if we already have a JWT token if not request new one
 *
 * @returns {(function(*, *, *): void)|*}
 */
module.exports.checkJWTToken = () => {
    return (req, res, next) => {
        if(!jwt) {
            axios.post(
                process.env.EXAMS_API_URI+'/login',
                {
                    name: process.env.EXAMS_API_USERNAME,
                    resto: process.env.RESTAURANT,
                    pwd: process.env.EXAMS_API_PASSWORD
                },
                {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            ).then(function (response) {
                jwt = response.data.token;
                next();
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            next();
        }
    }
}
