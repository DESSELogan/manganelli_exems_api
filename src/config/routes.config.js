const express = require('express');
const router = express.Router()

const StockController = require("../controllers/stock.controller");

const AuthMiddleware = require("../middleware/auth.middleware");

module.exports = router;

// Stocks routing
router.get('/stock/all_filtered', AuthMiddleware.checkJWTToken(), StockController.getAllFiltered);
