const axios = require("axios");
const moment = require("moment");
const fs = require('fs')
const csv = require('fast-csv');
const NodeCache = require( "node-cache" );
const productsCache = new NodeCache();

const {comparePrice} = require("../helpers/numbers.helper");

/**
 * Get all products with has stock and filter with DLC and price.
 *
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
exports.getAllFiltered = async (req, res) => {
    if (!fs.existsSync('files/stock.csv')) {
        return res.status(404).send('stock file not found');
    }

    // check if products exist in cache
    if(productsCache.has('products')) {
        let products = productsCache.get('products');

        /*
         * Read stock csv file
         * @Todo: see to remove code duplication
         */
        let stocks = new Map();
        fs.createReadStream('files/stock.csv')
            .pipe(csv.parse({ headers : true, delimiter:';' }))
            .on('error', error => console.error(error))
            .on('data', row => stocks.set(parseInt(row.IdProduct), parseInt(row.stock)))
            .on('end', () => {
                res.json(productsDataProcessing(products, stocks));
            });
    } else {
        axios.get(process.env.EXAMS_API_URI+'/products',
            {
                headers: {
                    'Authorization': jwt,
                    'apiKey': process.env.EXAMS_API_KEY
                }
            }).then(function (response) {
            let products = response.data;
            productsCache.set( "products", products, 20000);

            /*
             * Read stock csv file
             * @Todo: see to remove code duplication
            */
            let stocks = new Map();
            fs.createReadStream('files/stock.csv')
                .pipe(csv.parse({ headers : true, delimiter:';' }))
                .on('error', error => console.error(error))
                .on('data', row => stocks.set(parseInt(row.IdProduct), parseInt(row.stock)))
                .on('end', () => {
                    res.json(productsDataProcessing(products, stocks));
                });
        }).catch(() => {
            return res.status(404).send('Cannot get products from API');
        });
    }
};

/**
 * Process products (check dlc, stocks,...) and return object with array of featuredProducts and standardProducts
 *
 * @param products Array of products got with cache or API
 * @param stocks   Map of stocks got with CSV in (files/stock.csv)
 * @returns {{featuredProducts: *[], standardProducts: *[]}}
 */
function productsDataProcessing(products, stocks) {
    let featuredProducts = [];
    let standardProducts = [];
    for(let product of products) {
        // check if product is in stock and has stock
        if(stocks.has(product.id) && stocks.get(product.id) > 0) {
            // add stock to product object
            product.stock = stocks.get(product.id);
            // check if product's dlc is close (j+3)
            if(moment().diff(moment(product.dlc), 'days') >= -2) {
                product.price = product.price - (product.price * .30)
                featuredProducts.push(product);
                continue;
            }

            standardProducts.push(product);
        }
    }

    // sort product by price asc (see: helpers/numbers.helper.js)
    featuredProducts.sort(comparePrice);
    standardProducts.sort(comparePrice);

    return {featuredProducts, standardProducts};
}


