require('dotenv').config();

const express = require('express');
const routes = require('./src/config/routes.config');

const app = express();
app.use(express.json());
app.use('/api', routes);
app.listen(3000, () => console.log('serveur started at port :3000'));
